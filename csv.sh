#!/bin/bash
cd "$(dirname "$0")"

source _env.sh

#date=$(date +%Y%m%d_%H%M)
date=$(date +%Y%m%d)

for filename in sparql/*.sparql; do

    echo $filename

    key=${filename##*/}
    key=${key%.*}

    tfile=$(mktemp -u)
    query=$(cat $filename)

    code=$(curl -X POST -s -w "%{http_code}" -H "Accept: text/csv" \
        -H "X-BIGDATA-MAX-QUERY-MILLIS: 600000" \
        --data-urlencode "query=${query}" \
        -o $tfile \
        $SPARQL_ENDPOINT)

    if [ $code != 200 ]
    then
        echo "http error : ${code}" >&2
        continue
    fi

    if grep -q "Caused by:" $tfile
    then
        echo "java error" >&2
        continue
    fi

    curl -s -X POST -H "Accept: application/json" \
        -H "X-API-KEY: ${DATAGOUV_APIKEY}" \
        -F "file=@${tfile};filename=datatourisme.${key}.${date}.csv" \
        "https://www.data.gouv.fr/api/1/datasets/${DATAGOUV_DATASET}/upload/" > "logs/fma.log"

done
